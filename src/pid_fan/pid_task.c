/*
 * pid_task.c
 *
 *  Created on: 2022年9月25日
 *      Author: user1
 */

#include "pid_task.h"
#include "FreeRTOS.h"
#include "task.h"
#include "acc_task.h"
#include "motor_task.h"
#include "nrfx.h"

#define TASK_PRIORITY           2
#define TASK_STACK_SIZE     512

#define PID_CYCLE           100 // ms

static TaskHandle_t task_handle;
static StackType_t task_stack[TASK_STACK_SIZE / sizeof(StackType_t)];
static StaticTask_t task_obj;

static void task_run(void *arg);
static void pid_run();

void pid_create_task()
{
    task_handle = xTaskCreateStatic(task_run, "pid", sizeof(task_stack) / sizeof(StackType_t), NULL, TASK_PRIORITY,
                                    task_stack, &task_obj);
}

static void task_run(void *arg)
{

    for(;;)
    {
        pid_run();
        vTaskDelay(pdMS_TO_TICKS(PID_CYCLE));
    }
}

static void pid_run()
{

    int16_t acc_x, acc_y;
    acc_read(&acc_x, &acc_y, NULL);

    __NOP();
}
