/*
 * i2c_task.c
 *
 *  Created on: 2022年9月24日
 *      Author: user1
 */

#include "motor_task.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "nrf_gpio.h"
#include "nrf_twim.h"
#include "nrfx_irqs.h"

#define SCL_PIN             NRF_GPIO_PIN_MAP(0, 26)
#define SDA_PIN             NRF_GPIO_PIN_MAP(1, 0)

#define I2C_ADDR            0x40
#define TWIMx               NRF_TWIM1
#define TWIMx_IRQn          SPIM1_SPIS1_TWIM1_TWIS1_SPI1_TWI1_IRQn

#define REG_MODE1           0
#define REG_PRE_SCALE       254
#define REG_LED0_ON_L       6

#define TASK_PRIO           2
#define TASK_STACK_SIZE     512

static TaskHandle_t task_handle;
static StackType_t task_stack[TASK_STACK_SIZE / sizeof(StackType_t)];
static StaticTask_t task_obj;

static SemaphoreHandle_t sync_semphr;
static StaticSemaphore_t sync_semphr_obj;

static SemaphoreHandle_t i2c_semphr;
static StaticSemaphore_t i2c_semphr_obj;

static void task_run(void *arg);

static void i2c_config();
static void i2c_init();
static void i2c_write_sync(const uint8_t *buf, uint8_t len);
static void i2c_write_buf(const uint8_t *buf, uint8_t len);
static void set_pwm(uint16_t value, uint8_t *buf);

void motor_create_task()
{
    task_handle = xTaskCreateStatic(task_run, "motor", sizeof(task_stack) / sizeof(StackType_t), NULL, TASK_PRIO,
                                    task_stack, &task_obj);

    sync_semphr = xSemaphoreCreateBinaryStatic(&sync_semphr_obj);
    i2c_semphr = xSemaphoreCreateBinaryStatic(&i2c_semphr_obj);

}

void motor_run(motor_id_t motor, int32_t power)
{

    bool pos = true;
    if(power < 0)
    {
        pos = false;
        power = -power;
    }

    if(power > MOTOR_MAX_POWER)
    {
        power = MOTOR_MAX_POWER;
    }

    uint16_t power1 = (uint16_t) power;

    uint8_t buf[9];
    buf[0] = REG_LED0_ON_L + (motor - M1A) * 8;

    if(pos)
    {
        set_pwm(power1, buf + 1);
        set_pwm(0, buf + 5);
    }
    else
    {
        set_pwm(0, buf + 1);
        set_pwm(power1, buf + 5);
    }

    i2c_write_sync(buf, sizeof(buf));

}

static void set_pwm(uint16_t value, uint8_t *buf)
{

    buf[0] = 0; // LEDx_ON_L
    buf[1] = 0; // LEDx_ON_H
    buf[2] = 0xff & (value); // LEDx_OFF_L
    buf[3] = 0xff & (value >> 8); // LEDx_OFF_H
}

static void task_run(void *arg)
{

    i2c_config();
    i2c_init();

    // Allow client call only when init finished
    xSemaphoreGive(sync_semphr);

//    for(;;)
//    {
//        i2c_run_motor(M1A, 3200);
//
//        vTaskDelay(pdMS_TO_TICKS(5000));
//
//        i2c_stop_motor(M1A);
//
//        vTaskDelay(pdMS_TO_TICKS(5000));
//    }

    for(;;)
    {
        taskYIELD();
    }

}

static void i2c_write_sync(const uint8_t *buf, uint8_t len)
{
    // This function is to be called by clients

    xSemaphoreTake(sync_semphr, portMAX_DELAY);
    i2c_write_buf(buf, len);
    xSemaphoreGive(sync_semphr);
}

void nrfx_twim_1_irq_handler()
{

    BaseType_t x = pdFALSE;

    if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_LASTTX))
    {
        nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_LASTTX);
        nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STOP);
    }
    else if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_STOPPED))
    {
        nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_STOPPED);
        xSemaphoreGiveFromISR(i2c_semphr, &x);
        portYIELD_FROM_ISR(x);
    }
    else if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_ERROR))
    {
        nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_ERROR);

        uint32_t err = nrf_twim_errorsrc_get_and_clear(TWIMx);

        if(err & NRF_TWIM_ERROR_OVERRUN)
        {
            // Nothing
        }
        if(err & NRF_TWIM_ERROR_ADDRESS_NACK)
        {
            // Nothing
        }
        if(err & NRF_TWIM_ERROR_DATA_NACK)
        {
            // Nothing
        }

        nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STOP);
    }
    else
    {
        for(;;)
        {
            __NOP();
        }
    }

}

static void i2c_write_buf(const uint8_t *buf, uint8_t len)
{
    nrf_twim_tx_buffer_set(TWIMx, buf, len);
    nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STARTTX);

    // Block until ISR done
    xSemaphoreTake(i2c_semphr, portMAX_DELAY);
}

static void i2c_init()
{
    uint8_t buf1[2];

    // Pre-scale = 30 -> output freq 200 Hz
    buf1[0] = REG_PRE_SCALE;
    buf1[1] = 30;
    i2c_write_buf(buf1, sizeof(buf1));

    // Auto-Incr, Normal mode
    buf1[0] = REG_MODE1;
    buf1[1] = 0x20;
    i2c_write_buf(buf1, sizeof(buf1));

    // It takes 500us max for internal oscillator to run
    vTaskDelay(pdMS_TO_TICKS(1));

}

static void i2c_config()
{

    NVIC_SetPriority( TWIMx_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 5, 0));
    NVIC_EnableIRQ(TWIMx_IRQn);

    nrf_gpio_cfg(SCL_PIN, NRF_GPIO_PIN_DIR_INPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1,
                 NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(SDA_PIN, NRF_GPIO_PIN_DIR_INPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1,
                 NRF_GPIO_PIN_NOSENSE);

    nrf_twim_pins_set( TWIMx, SCL_PIN, SDA_PIN);
    nrf_twim_frequency_set( TWIMx, NRF_TWIM_FREQ_400K);
    nrf_twim_address_set(TWIMx, I2C_ADDR);
    nrf_twim_int_enable(TWIMx, NRF_TWIM_INT_STOPPED_MASK | NRF_TWIM_INT_ERROR_MASK | NRF_TWIM_INT_LASTTX_MASK);
    nrf_twim_enable(TWIMx);
}
