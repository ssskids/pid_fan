/*
 * blink_task.c
 *
 *  Created on: 2022年9月24日
 *      Author: user1
 */

#include "blink_task.h"
#include "FreeRTOS.h"
#include "task.h"
#include "nrf_gpio.h"

#define ROW1_PIN        NRF_GPIO_PIN_MAP(0, 21)
#define COL1_PIN        NRF_GPIO_PIN_MAP(0, 28)

#define TASK_PRIO       2

static TaskHandle_t task_handle;
static StackType_t task_stack[128 / sizeof(StackType_t)];
static StaticTask_t task_obj;

/**
 * @brief  Function implementing the blink thread.
 * @param  arg: Not used
 * @retval None
 */
static void task_run(void *arg)
{
    nrf_gpio_pin_write(ROW1_PIN, 0);
    nrf_gpio_cfg_output(ROW1_PIN);

    nrf_gpio_pin_write(COL1_PIN, 1);
    nrf_gpio_cfg_output(COL1_PIN);

    nrf_gpio_pin_write(ROW1_PIN, 1);

    /* Infinite loop */
    for(;;)
    {
        nrf_gpio_pin_toggle(COL1_PIN);
        vTaskDelay(pdMS_TO_TICKS(500));
    }

}

void blink_create_task()
{
    task_handle = xTaskCreateStatic(task_run, "blink", sizeof(task_stack) / sizeof(StackType_t), NULL, TASK_PRIO,
                                    task_stack, &task_obj);
}
