/*
 * pid_task.h
 *
 *  Created on: 2022年9月25日
 *      Author: user1
 */

#ifndef PID_TASK_H_
#define PID_TASK_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

void pid_create_task();

#endif /* PID_TASK_H_ */
