/*
 * acc_task.c
 *
 *  Created on: 2022年9月25日
 *      Author: user1
 */

#include "acc_task.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "nrf_gpio.h"
#include "nrf_twim.h"
#include "nrfx_irqs.h"

#define TASK_PRIORITY           2
#define TASK_STACK_SIZE     512

#define SCL_PIN             NRF_GPIO_PIN_MAP(0, 8)
#define SDA_PIN             NRF_GPIO_PIN_MAP(0, 16 )

#define I2C_ADDR            0x19
#define TWIMx               NRF_TWIM0
#define TWIMx_IRQn          SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQn
#define TWIMx_Handler       nrfx_twim_0_irq_handler

#define REG_WHO_AM_I        0x0f
#define REG_CTRL_REG1       0x20
#define REG_CTRL_REG4       0x23
#define REG_OUT_X_L         0x28

#define ACC_DATA_BITS       12

typedef enum
{
    I2C_READ, //
    I2C_WRITE, //
} i2c_op_t;

static TaskHandle_t task_handle;
static StackType_t task_stack[TASK_STACK_SIZE / sizeof(StackType_t)];
static StaticTask_t task_obj;

static SemaphoreHandle_t sync_semphr;
static StaticSemaphore_t sync_semphr_obj;

static SemaphoreHandle_t i2c_semphr;
static StaticSemaphore_t i2c_semphr_obj;

static volatile i2c_op_t i2c_op = 0;

static void task_run(void *arg);

static void i2c_config();
static void i2c_init();
static void i2c_write_sync(uint8_t *buf, uint8_t len);
static void i2c_read_sync(uint8_t *buf, uint8_t len);
static void i2c_write(uint8_t *buf, uint8_t len);
static void i2c_read(uint8_t *buf, uint8_t len);

static int16_t get_acc_value(uint8_t lo, uint8_t hi, uint32_t bits);

void acc_create_task()
{
    task_handle = xTaskCreateStatic(task_run, "acc", sizeof(task_stack) / sizeof(StackType_t), NULL, TASK_PRIORITY,
                                    task_stack, &task_obj);

    sync_semphr = xSemaphoreCreateBinaryStatic(&sync_semphr_obj);
    i2c_semphr = xSemaphoreCreateBinaryStatic(&i2c_semphr_obj);
}

static void task_run(void *arg)
{

    i2c_config();
    i2c_init();

    // Allow client call only when init finished
    xSemaphoreGive(sync_semphr);

//    for(;;)
//    {
//
//        int16_t x, y, z;
//        acc_read(&x, &y, &z);
//
//        __NOP();
//
//        vTaskDelay(pdMS_TO_TICKS(1000));
//    }

    for(;;)
    {
        taskYIELD();
    }

}

void acc_read(int16_t *x, int16_t *y, int16_t *z)
{

    // X_L, X_H, Y_L, Y_H, Z_L, Z_H
    uint8_t buf[7];
    buf[0] = REG_OUT_X_L;
    i2c_read_sync(buf, sizeof(buf));

    if( NULL != x)
    {
        *x = get_acc_value(buf[1], buf[2], ACC_DATA_BITS);
    }
    if( NULL != y)
    {
        *y = get_acc_value(buf[3], buf[4], ACC_DATA_BITS);
    }
    if( NULL != z)
    {
        *z = get_acc_value(buf[5], buf[6], ACC_DATA_BITS);
    }

}

static int16_t get_acc_value(uint8_t lo, uint8_t hi, uint32_t bits)
{

    uint16_t v = ((0xff & hi) << 8) | (0xff & lo);
    uint32_t msb = (0x8000 & v) >> 15;
    v = v >> (16 - bits);

    if(0 != msb)
    {
        // Sign extend

        uint32_t msb_bits = 0xffff;
        msb_bits = (msb_bits >> bits) << bits;
        v |= msb_bits;
    }

    return (int16_t) v;
}

static void i2c_write_sync(uint8_t *buf, uint8_t len)
{
    // This function is to be called by clients

    xSemaphoreTake(sync_semphr, portMAX_DELAY);
    i2c_write(buf, len);
    xSemaphoreGive(sync_semphr);
}

static void i2c_read_sync(uint8_t *buf, uint8_t len)
{
    // This function is to be called by clients

    xSemaphoreTake(sync_semphr, portMAX_DELAY);
    i2c_read(buf, len);
    xSemaphoreGive(sync_semphr);
}

void TWIMx_Handler()
{

    BaseType_t x = pdFALSE;

    if(I2C_WRITE == i2c_op)
    {

        if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_LASTTX))
        {
            nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_LASTTX);
            nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STOP);
        }
        else if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_STOPPED))
        {
            nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_STOPPED);
            xSemaphoreGiveFromISR(i2c_semphr, &x);
            portYIELD_FROM_ISR(x);
        }
        else if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_ERROR))
        {
            nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_ERROR);

            uint32_t err = nrf_twim_errorsrc_get_and_clear(TWIMx);

            if(err & NRF_TWIM_ERROR_OVERRUN)
            {
                // Nothing
            }
            if(err & NRF_TWIM_ERROR_ADDRESS_NACK)
            {
                // Nothing
            }
            if(err & NRF_TWIM_ERROR_DATA_NACK)
            {
                // Nothing
            }

            nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STOP);
        }
        else
        {
            for(;;)
            {
                __NOP();
            }
        }

    }
    else if(I2C_READ == i2c_op)
    {
        if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_LASTTX))
        {
            nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_LASTTX);
            nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STARTRX);
        }
        else if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_LASTRX))
        {
            nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_LASTRX);
            nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STOP);
        }
        else if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_STOPPED))
        {
            nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_STOPPED);
            xSemaphoreGiveFromISR(i2c_semphr, &x);
            portYIELD_FROM_ISR(x);
        }
        else if(nrf_twim_event_check(TWIMx, NRF_TWIM_EVENT_ERROR))
        {
            nrf_twim_event_clear(TWIMx, NRF_TWIM_EVENT_ERROR);

            uint32_t err = nrf_twim_errorsrc_get_and_clear(TWIMx);

            if(err & NRF_TWIM_ERROR_OVERRUN)
            {
                // Nothing
            }
            if(err & NRF_TWIM_ERROR_ADDRESS_NACK)
            {
                // Nothing
            }
            if(err & NRF_TWIM_ERROR_DATA_NACK)
            {
                // Nothing
            }

            nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STOP);
        }
        else
        {
            for(;;)
            {
                __NOP();
            }
        }
    }
    else
    {
        for(;;)
        {
            __NOP();
        }
    }

}

static void i2c_write(uint8_t *buf, uint8_t len)
{
    // Auto increment
    if(len <= 2)
    {
        buf[0] = (uint8_t) (0x7f & buf[0]);
    }
    else
    {
        buf[0] = (uint8_t) (0x80 | buf[0]);
    }

    i2c_op = I2C_WRITE;

    nrf_twim_tx_buffer_set(TWIMx, buf, len);
    nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STARTTX);

    // Block until ISR done
    xSemaphoreTake(i2c_semphr, portMAX_DELAY);
}

static void i2c_read(uint8_t *buf, uint8_t len)
{

    // Auto increment
    if(len <= 2)
    {
        buf[0] = (uint8_t) (0x7f & buf[0]);
    }
    else
    {
        buf[0] = (uint8_t) (0x80 | buf[0]);
    }

    i2c_op = I2C_READ;

    // Write reg address
    nrf_twim_tx_buffer_set(TWIMx, buf, 1);
    nrf_twim_rx_buffer_set(TWIMx, buf + 1, len - 1);
    nrf_twim_task_trigger(TWIMx, NRF_TWIM_TASK_STARTTX);

    // Block until ISR done
    xSemaphoreTake(i2c_semphr, portMAX_DELAY);

}

static void i2c_init()
{
    uint8_t buf1[2];

    // +/-2 g, HR (12 bits data output)
    buf1[0] = REG_CTRL_REG4;
    buf1[1] = 0x08;
    i2c_write(buf1, sizeof(buf1));

    // 100 Hz, X/Y/Z enabled
    buf1[0] = REG_CTRL_REG1;
    buf1[1] = 0x57;
    i2c_write(buf1, sizeof(buf1));

    //
    buf1[0] = REG_WHO_AM_I;
    buf1[1] = 0;
    i2c_read(buf1, sizeof(buf1));

    if(0x33 != buf1[1])
    {
        for(;;)
        {
            __NOP();
        }
    }

}

static void i2c_config()
{

    NVIC_SetPriority( TWIMx_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 5, 0));
    NVIC_EnableIRQ(TWIMx_IRQn);

    nrf_gpio_cfg(SCL_PIN, NRF_GPIO_PIN_DIR_INPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1,
                 NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(SDA_PIN, NRF_GPIO_PIN_DIR_INPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_S0D1,
                 NRF_GPIO_PIN_NOSENSE);

    nrf_twim_pins_set( TWIMx, SCL_PIN, SDA_PIN);
    nrf_twim_frequency_set( TWIMx, NRF_TWIM_FREQ_400K);
    nrf_twim_address_set(TWIMx, I2C_ADDR);
    nrf_twim_int_enable( TWIMx, NRF_TWIM_INT_STOPPED_MASK | //
                                NRF_TWIM_INT_ERROR_MASK |  //
                                NRF_TWIM_INT_LASTTX_MASK |  //
                                NRF_TWIM_INT_LASTRX_MASK);
    nrf_twim_enable(TWIMx);
}

