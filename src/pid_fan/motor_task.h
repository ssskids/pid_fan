/*
 * i2c_task.h
 *
 *  Created on: 2022年9月24日
 *      Author: user1
 */

#ifndef MOTOR_TASK_H_
#define MOTOR_TASK_H_

#include <stdint.h>
#include <stdbool.h>

#define MOTOR_MAX_POWER             4095

typedef enum
{
    M1A = 0, //
    M1B, //
    M2A, //
    M2B, //
} motor_id_t;

void motor_create_task();

void motor_run(motor_id_t motor, int32_t power);

static void motor_stop(motor_id_t motor)
{
    motor_run(motor, 0);
}

#endif /* MOTOR_TASK_H_ */
