/*
 * acc_task.h
 *
 *  Created on: 2022年9月25日
 *      Author: user1
 */

#ifndef ACC_TASK_H_
#define ACC_TASK_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

void acc_create_task();

void acc_read(int16_t *x, int16_t *y, int16_t *z);

#endif /* ACC_TASK_H_ */
