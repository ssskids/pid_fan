/*
 * blink_task.h
 *
 *  Created on: 2022年9月24日
 *      Author: user1
 */

#ifndef BLINK_TASK_H_
#define BLINK_TASK_H_

#include <stdint.h>
#include <stdbool.h>

void blink_create_task();

#endif /* BLINK_TASK_H_ */
